# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-07 12:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stepchallenge', '0005_auto_20170907_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='email_confirmed',
            field=models.BooleanField(default=False),
        ),
    ]
