from django.contrib import messages
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum, Count
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.template.defaulttags import register
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

import logging
import datetime

from .models import StepCount, Team, Profile
from .forms import SignUpForm, StepForm, PastStepForm, SelectTeam, NewTeam, UpdateEmailSettings
from .tokens import account_activation_token

logger = logging.getLogger(__name__)

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.simple_tag
def get_username(user_id):
    try:
        return User.objects.get(id=user_id).username
    except User.DoesNotExist:
        return 'Unknown'

@register.simple_tag
def get_teamname(team_id):
    try:
        return Team.objects.get(id=team_id).team_name
    except Team.DoesNotExist:
        return 'Unknown'

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            return HttpResponseRedirect(reverse('user', args=[request.user.id]))
    else:
        form = PasswordChangeForm(request.user)

    return render(request, 'change_password.html', {
        'form': form
    })

def index(request):
    latest_steps = StepCount.objects.order_by('-step_date')[:5]

    top_users = StepCount.objects.values('user').annotate(steps=Sum('steps')).order_by('-steps')[:5]

    top_teams = StepCount.objects.values('user__profile__team').annotate(steps=Sum('steps')).order_by('-steps')[:5]

    total_steps = StepCount.objects.aggregate(Sum('steps'))['steps__sum']
    users = StepCount.objects.all().aggregate(Count('user', distinct=True))['user__count']


    context = {
       'latest_steps_list': latest_steps,
       'top_users': top_users,
       'top_teams': top_teams,
       'steps': total_steps,
       'users': users,
    }

    if request.user.is_authenticated():
      try:
        myentry = StepCount.objects.filter(user=request.user).order_by('-step_date')[0]
        context['mydate'] = myentry.step_date
        context['mysteps'] = myentry.steps
      except IndexError:
        pass

    return render(request, 'stepchallenge/index.html', context)

def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')

def team_detail(request, team_id):
    team = get_object_or_404(Team, pk=team_id)

    users = {}

    for p in team.user.all():
       users[p.user.id] = StepCount.objects.filter(user=p.user).aggregate(Sum('steps'))['steps__sum']

    return render(request, 'stepchallenge/team_detail.html', {'t': team, 'users': users})

def user_detail(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    latest_steps = StepCount.objects.filter(user=user).order_by('-step_date')
    steps = latest_steps.aggregate(Sum('steps'))['steps__sum']

    if request.user.is_authenticated() and request.user == user:
      self = True
      email = request.user.email
      optin = request.user.profile.email_optin
    else:
      self = False
      email = None
      optin = None

    context = {
       'latest_steps_list': latest_steps[:10],
       'u': user,
       'steps': steps,
       'self': self,
       'email': email,
       'optin': optin
    }

    return render(request, 'stepchallenge/user_detail.html', context)

def register(request):
    if request.method == 'POST':
       form = StepForm(request.POST)
       if form.is_valid():
           s = StepCount(user=request.user,
                     steps=form.cleaned_data['steps'],
                     step_date=(datetime.datetime.now()-datetime.timedelta(hours=18)).date())
           s.save()
           return HttpResponseRedirect(reverse('user', args=[request.user.id]))
    else:
       form = StepForm()
       return render(request, 'stepchallenge/register.html', {'date': (datetime.datetime.now()-datetime.timedelta(hours=18)).date(), 'form': form} )

def register_past(request):
    if request.method == 'POST':
       form = PastStepForm(request.POST)
       if form.is_valid():
           s = StepCount(user=request.user,
                     steps=form.cleaned_data['steps'],
                     step_date=form.cleaned_data['step_date'])
           s.save()
           return HttpResponseRedirect(reverse('user', args=[request.user.id]))
    else:
       form = PastStepForm()
       return render(request, 'stepchallenge/register.html', {'form': form} )

def updateemailsettings(request):
    if request.method == 'POST':
       form = UpdateEmailSettings(request.POST)
       if form.is_valid():
           user = User.objects.get(pk=request.user.id)
           user.email = form.cleaned_data.get('email')
           user.profile.email_optin = form.cleaned_data.get('email_optin')
           user.save()

           return HttpResponseRedirect(reverse('user', args=[request.user.id]))
    else:
        form = UpdateEmailSettings(initial={'email': request.user.email, 'email_optin': request.user.profile.email_optin})
    return render(request, 'updatesettings.html', {'form': form})
    

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit = False)
            user.is_active = False
            user.save()
            user.profile.email_optin = form.cleaned_data.get('email_optin')
            user.save()
            current_site = get_current_site(request)
            subject = 'Step this way! Activate Your Python March Account'
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message, 'no-reply@pythonmarch.party')
            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def jointeam(request):
    if request.method == 'POST':
       form = SelectTeam(request.POST)
       if form.is_valid():
          team = get_object_or_404(Team, pk=form.cleaned_data.get('team'))
          #
          # Okay, we're joining another team. First check if we're
          # the lead of our current team, in which case we need to
          # update to say the team has no lead.
          #
          if request.user.profile.team:
            oldteam = get_object_or_404(Team, pk=request.user.profile.team.id)
            if oldteam == team:
               # No change requested
               return HttpResponseRedirect(reverse('jointeam'))

            if oldteam.team_lead.id == request.user.id:
               oldteam.team_lead = None
               oldteam.save()

            #
            # Check if the old team has any other members, else remove it.
            #
            if User.objects.filter(profile__team=oldteam).count() <= 1:
               oldteam.delete()

          request.user.profile.team = team
          request.user.profile.save()
          return HttpResponseRedirect(reverse('user', args=[request.user.id]))
    else:
       form = SelectTeam()

    if request.user.profile.team:
       team = request.user.profile.team.team_name

       if request.user.profile.team.team_lead == request.user:
          team_l = True
       else:
          team_l = False
    else:
       team_l = False
       team = None

    return render(request, 'stepchallenge/jointeam.html', {'form': form, 'team': team, 'team_lead': team_l })

def newteam(request):
    if request.method == 'POST':
       form = NewTeam(request.POST)
       if form.is_valid():
          #
          # Check and remove leader role in other team
          #
          if request.user.profile.team:
            oldteam = get_object_or_404(Team, pk=request.user.profile.team.id)

            if oldteam.team_lead.id == request.user.id:
               oldteam.team_lead = None
               oldteam.save()

          team = Team(team_name = form.cleaned_data.get('team_name'),
                      team_lead = request.user)
          team.save()
          request.user.profile.team = team
          request.user.profile.save()

          return HttpResponseRedirect(reverse('user', args=[request.user.id]))
    else:
       form = NewTeam()

    if request.user.profile.team and request.user.profile.team.team_lead == request.user:
       team_lead = True
    else:
       team_lead = False

    return render(request, 'stepchallenge/newteam.html', {'form': form, 'team_lead': team_lead})

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        #
        # This might seem odd, but privacy_agreed is a required field, so
        # you can not go anywhere without agreeing :-)
        #
        user.profile.privacy_agreed = True
        user.save()
        login(request, user)
        return redirect('index')
    else:
        return render(request, 'account_activation_invalid.html')
