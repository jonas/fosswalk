from dal import autocomplete

from .models import Team

class TeamAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Team.objects.none()

        qs = Team.objects.filter(team_lead__isnull=False)

        if self.q:
            qs = qs.filter(team_name__istartswith=self.q)

        return qs
